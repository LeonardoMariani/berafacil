package com.example.leonardo.berafacil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class BancoController {

    private SQLiteDatabase db;
    private CriaBanco banco;

    public BancoController(Context context){
        banco = new CriaBanco(context);
    }

    public boolean insereDado(String nome,String tamanho, String nomeLocal, String custo, String latitude,
    String longitude){
        boolean okInserido = false;

        ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();

        valores.put(CriaBanco.NOME, nome);
        valores.put(CriaBanco.NOMELOCAL, nomeLocal);
        valores.put(CriaBanco.TAMANHO, tamanho);
        valores.put(CriaBanco.CUSTO, custo);
        valores.put(CriaBanco.LATITUDE, latitude);
        valores.put(CriaBanco.LONGITUDE, longitude);

        resultado = db.insert(CriaBanco.TABELA, null, valores);
        db.close();

        if(resultado != -1) okInserido = true;

        return  okInserido;
    }

    public ArrayList<Bera> carregaDados(String nome, String tamanho, String distancia, String latitude, String longitude ){

        Cursor cursor = null;
        ArrayList<Bera> lista = new ArrayList<Bera>();

        //nome = "B1";
        //tamanho = "1";
        db = banco.getReadableDatabase();
        String query = "SELECT * FROM " + CriaBanco.TABELA + " WHERE TAMANHO = " + tamanho;


        if(nome.length() > 0){
            query += " AND " + CriaBanco.NOME + " LIKE '%" + nome + "%' ";
        }
        cursor = db.rawQuery(query,null);

        if(cursor!=null){
            cursor.moveToFirst();

            while(!cursor.isAfterLast()){
                Bera bera = parseBera(cursor, distancia, latitude, longitude);

                if(bera != null) lista.add(bera);

                cursor.moveToNext();
            }
        }
        db.close();

        return lista;
    }

    private Bera parseBera(Cursor cursor, String distancia, String latitude, String longitude) {
        Bera bera = new Bera();

        double lat1 = Double.valueOf(latitude);
        double long1 = Double.valueOf(longitude);

        double lat2 = Double.valueOf(cursor.getString(5));
        double long2 = Double.valueOf(cursor.getString(6));

        double diferenca = 0.0;

        diferenca = getDistance(lat1, long1, lat2, long2);

        if(diferenca < Double.valueOf(distancia)){
            bera.setNome(cursor.getString(1));
            bera.setNomeLocal(cursor.getString(2));
            bera.setCusto(Double.valueOf(cursor.getString(3)));
            bera.setTamanho(Integer.valueOf(cursor.getString(4)));
            bera.setLatitude(cursor.getString(5));
            bera.setLongitude(cursor.getString(6));
        }else{
            bera = null;
        }
        return bera;
    }


    public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;

        dist = dist * 1.609344;//KM

        dist = dist * 1000;//Metros

        return (dist);
    }
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }

    public void limpaDados() {

        db = banco.getWritableDatabase();
        banco.onUpgrade(db,1,1);
        db.close();
    }
}
