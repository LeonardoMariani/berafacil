package com.example.leonardo.berafacil;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CriaBanco extends SQLiteOpenHelper {
    public static final String NOME_BANCO = "banco.db";
    public static final String TABELA = "Cervejas";
    public static final String ID = "id";
    public static final String NOME = "Nome";
    public static final String NOMELOCAL = "NomeLocal";
    public static final String CUSTO = "Custo";
    public static final String TAMANHO = "Tamanho";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final int VERSAO = 1;


    public CriaBanco(Context context){
        super(context, NOME_BANCO,null,VERSAO);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        String sql = "CREATE TABLE "+TABELA+"("
                + ID + " integer primary key autoincrement, "
                + NOME + " text, "
                + NOMELOCAL + " text, "
                + CUSTO + " text, "
                + TAMANHO + " text, "
                + LATITUDE + " text, "
                + LONGITUDE + " text)";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABELA);
        onCreate(db);
    }
}