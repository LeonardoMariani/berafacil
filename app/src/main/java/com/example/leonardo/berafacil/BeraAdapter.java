package com.example.leonardo.berafacil;

import android.app.Activity;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by Leonardo on 20/06/2018.
 */

public class BeraAdapter extends BaseAdapter {

    private List<Bera> listaBera;
    private Activity activity;

    public BeraAdapter(Activity activity, List<Bera> listaBera) {
        this.listaBera = listaBera;
        this.activity = activity;
    }


    @Override
    public int getCount() {
        return listaBera.size();
    }

    @Override
    public Object getItem(int i) {
        return listaBera.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        View view = activity.getLayoutInflater().inflate(R.layout.listaberas, viewGroup , false);
        Bera bera = listaBera.get(i);

        TextView nome = (TextView) view.findViewById(R.id.textViewNomeListBeras);
        TextView nomeLocal = (TextView) view.findViewById(R.id.textViewNomeLocalListBeras);
        TextView custo = (TextView) view.findViewById(R.id.textViewCustoListBeras);
        TextView latitude = (TextView) view.findViewById(R.id.textViewLatListBeras);
        TextView longitude = (TextView) view.findViewById(R.id.textViewLongListBeras);
        ImageView tamanho = (ImageView) view.findViewById(R.id.imageViewListBeras);

        nome.setText(bera.getNome());
        nomeLocal.setText(bera.getNomeLocal());
        custo.setText("$" +String.valueOf(bera.getCusto()));
        latitude.setText(String.valueOf(bera.getLatitude()));
        longitude.setText(String.valueOf(bera.getLongitude()));

        if(bera.getTamanho() == 1){
            tamanho.setImageResource(R.drawable.latinha2);
        }else if(bera.getTamanho() == 2){
            tamanho.setImageResource(R.drawable.lata2);
        }else if(bera.getTamanho() == 3){
            tamanho.setImageResource(R.drawable.latao3);
        }else if(bera.getTamanho() == 4){
            tamanho.setImageResource(R.drawable.long2);
        }else if(bera.getTamanho() == 5){
            tamanho.setImageResource(R.drawable.garrafa02);
        }else if(bera.getTamanho() == 6){
            tamanho.setImageResource(R.drawable.litrao02);
        }else if(bera.getTamanho() == 7){
            tamanho.setImageResource(R.drawable.barril02);
        }

        return view;
    }
}
