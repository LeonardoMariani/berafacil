package com.example.leonardo.berafacil;

import android.Manifest;
import android.accessibilityservice.AccessibilityService;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private int CODE_TELA_PESQUISAR = 1;
    static final int REQUEST_LOCATION = 1;
    LocationManager locationManager;
    public GpsTracker gps;
    private double latitude;
    private double longitude;
    private double latitudePesq;
    private double longitudePesq;
    private String distancia = "10";
    private BancoController bancoController;
    private ImageView latinha, lata, latao, longneck, garrafa, litrao, barril;
    private int tamanho = 1;
    private EditText editTextNome;
    private RadioButton radioButton10, radioButton40, radioButton100, radioButton150;
    private ArrayList<Bera> listaBeras;

    private double latB2;
    private double lonB2;
    private double latB3;
    private double lonB3;
    private double latB4;
    private double lonB4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bancoController = new BancoController(getBaseContext());
        gps = new GpsTracker(this);

        Button btnPesquisar = (Button) findViewById(R.id.btnPesquisar);
        latinha = (ImageView) findViewById(R.id.imageView);
        lata = (ImageView) findViewById(R.id.imageView2);
        latao = (ImageView) findViewById(R.id.imageView3);
        longneck = (ImageView) findViewById(R.id.imageView4);
        garrafa = (ImageView) findViewById(R.id.imageView6);
        litrao = (ImageView) findViewById(R.id.imageView7);
        barril = (ImageView) findViewById(R.id.imageView8);
        editTextNome = (EditText) findViewById(R.id.editTextNome);
        radioButton10 = (RadioButton) findViewById(R.id.radioButton10);
        radioButton40 = (RadioButton) findViewById(R.id.radioButton40);
        radioButton100 = (RadioButton) findViewById(R.id.radioButton100);
        radioButton150 = (RadioButton) findViewById(R.id.radioButton150);

        listaBeras = new ArrayList<>();

        bancoController.limpaDados();
        criarDados();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        getLocalizacao();
        setTamanhoInicial();
    }

    private void setTamanhoInicial() {
        if(latinha.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.latinha).getConstantState())){
            tamanho = 1;
            latinha.setImageResource(R.drawable.latinha2);
            lata.setImageResource(R.drawable.lata);
            latao.setImageResource(R.drawable.latao);
            longneck.setImageResource(R.drawable.longneck);
            garrafa.setImageResource(R.drawable.garrafa);
            litrao.setImageResource(R.drawable.litrao);
            barril.setImageResource(R.drawable.barril);
        }
    }

    private void criarDados() {
        //banquinho
        bancoController.insereDado("BANQUINHO","1", "La mesmo3","5.00",
                "-25.444937","-49.357369");
        //rotatoria
        bancoController.insereDado("rotatoria","2", "La mesmo3","5.00",
                "-25.444520","-49.358928");

    }

    public void pesquisar(View view) {

        getLocalizacao();

        Bera bera = new Bera();
        bera.setNome("B1");
        bera.setNomeLocal("L1");
        bera.setCusto(12.12);
        bera.setTamanho(1);
        bera.setLatitude("-25.402670");
        bera.setLongitude("-49.163460");
        listaBeras.add(bera);

        bera = new Bera();
        bera.setNome("B2");
        bera.setNomeLocal("L1");
        bera.setCusto(12.12);
        bera.setTamanho(7);
        bera.setLatitude("-25.402670");
        bera.setLongitude("-49.163460");
        listaBeras.add(bera);

        bera = new Bera();
        bera.setNome("B3");
        bera.setNomeLocal("L1");
        bera.setCusto(12.12);
        bera.setTamanho(4);
        bera.setLatitude("-25.402670");
        bera.setLongitude("-49.163460");
        listaBeras.add(bera);

        listaBeras.clear();
        listaBeras = bancoController.carregaDados(editTextNome.getText().toString(), String.valueOf(tamanho), distancia, String.valueOf(latitude), String.valueOf(longitude));

        if(listaBeras.size() > 0){
            Intent i = new Intent(this, ResultadoActivity.class);
            Bundle params = new Bundle();

            i.putExtra("listaBeras", (Serializable) listaBeras);
            startActivity(i);
        }else{
            Toast.makeText(getApplicationContext(), "Nem uma bera encontrada :( ", Toast.LENGTH_SHORT).show();

        }
    }
    private void getLocalizacao() {

        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

        } else {
            gps.showSettingsAlert();
        }
    }

    public void recipiente(View view) {

        if(latinha.isPressed()){
            if(latinha.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.latinha).getConstantState())){
                tamanho = 1;
                latinha.setImageResource(R.drawable.latinha2);
                lata.setImageResource(R.drawable.lata);
                latao.setImageResource(R.drawable.latao);
                longneck.setImageResource(R.drawable.longneck);
                garrafa.setImageResource(R.drawable.garrafa);
                litrao.setImageResource(R.drawable.litrao);
                barril.setImageResource(R.drawable.barril);
            }
        }else if(lata.isPressed()){
            if(lata.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.lata).getConstantState())){
                tamanho = 2;
                lata.setImageResource(R.drawable.lata2);
                latinha.setImageResource(R.drawable.latinha);
                latao.setImageResource(R.drawable.latao);
                longneck.setImageResource(R.drawable.longneck);
                garrafa.setImageResource(R.drawable.garrafa);
                litrao.setImageResource(R.drawable.litrao);
                barril.setImageResource(R.drawable.barril);
            }
        }else if(latao.isPressed()){
            if(latao.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.latao).getConstantState())){
                tamanho = 3;
                latao.setImageResource(R.drawable.latao3);
                latinha.setImageResource(R.drawable.latinha);
                lata.setImageResource(R.drawable.lata);
                longneck.setImageResource(R.drawable.longneck);
                garrafa.setImageResource(R.drawable.garrafa);
                litrao.setImageResource(R.drawable.litrao);
                barril.setImageResource(R.drawable.barril);
            }
        }else if(longneck.isPressed()){
            if(longneck.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.longneck).getConstantState())){
                tamanho = 4;
                longneck.setImageResource(R.drawable.long2);
                latinha.setImageResource(R.drawable.latinha);
                lata.setImageResource(R.drawable.lata);
                latao.setImageResource(R.drawable.latao);
                garrafa.setImageResource(R.drawable.garrafa);
                litrao.setImageResource(R.drawable.litrao);
                barril.setImageResource(R.drawable.barril);
            }
        }else if(garrafa.isPressed()){
            if(garrafa.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.garrafa).getConstantState())){
                tamanho = 5;
                garrafa.setImageResource(R.drawable.garrafa02);
                latinha.setImageResource(R.drawable.latinha);
                lata.setImageResource(R.drawable.lata);
                latao.setImageResource(R.drawable.latao);
                longneck.setImageResource(R.drawable.longneck);
                litrao.setImageResource(R.drawable.litrao);
                barril.setImageResource(R.drawable.barril);
            }
        }else if(litrao.isPressed()){
            if(litrao.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.litrao).getConstantState())){
                tamanho = 6;
                litrao.setImageResource(R.drawable.litrao02);
                latinha.setImageResource(R.drawable.latinha);
                lata.setImageResource(R.drawable.lata);
                latao.setImageResource(R.drawable.latao);
                longneck.setImageResource(R.drawable.longneck);
                garrafa.setImageResource(R.drawable.garrafa);
                barril.setImageResource(R.drawable.barril);
            }
        }else if(barril.isPressed()){
            if(barril.getDrawable().getConstantState().equals(getResources().getDrawable(R.drawable.barril).getConstantState())){
                tamanho = 7;
                barril.setImageResource(R.drawable.barril02);
                latinha.setImageResource(R.drawable.latinha);
                lata.setImageResource(R.drawable.lata);
                latao.setImageResource(R.drawable.latao);
                longneck.setImageResource(R.drawable.longneck);
                garrafa.setImageResource(R.drawable.garrafa);
                litrao.setImageResource(R.drawable.litrao);
            }
        }
    }

    public void mudaEstado(View view) {

        if(radioButton10.isPressed()){
            distancia = "10";
            radioButton10.setChecked(true);
            radioButton40.setChecked(false);
            radioButton100.setChecked(false);
            radioButton150.setChecked(false);
        }else if(radioButton40.isPressed()){
            distancia = "40";
            radioButton10.setChecked(false);
            radioButton40.setChecked(true);
            radioButton100.setChecked(false);
            radioButton150.setChecked(false);

        }else if(radioButton100.isPressed()){
            distancia = "100";
            radioButton10.setChecked(false);
            radioButton40.setChecked(false);
            radioButton100.setChecked(true);
            radioButton150.setChecked(false);

        }else if(radioButton150.isPressed()){
            distancia = "20000";
            radioButton10.setChecked(false);
            radioButton40.setChecked(false);
            radioButton100.setChecked(false);
            radioButton150.setChecked(true);
        }
    }
}
