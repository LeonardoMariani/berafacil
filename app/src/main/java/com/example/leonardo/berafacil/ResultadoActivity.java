package com.example.leonardo.berafacil;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ResultadoActivity extends AppCompatActivity {

    ListView listViewBeras;
    Intent i;
    Bundle params;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        listViewBeras = (ListView) findViewById(R.id.listViewBeras);

        Intent i = getIntent();
        List<Bera> listaBeras = (List<Bera>) i.getSerializableExtra("listaBeras");

        BeraAdapter adapter = new BeraAdapter(this,listaBeras);
        listViewBeras.setAdapter(adapter);



    }
}
